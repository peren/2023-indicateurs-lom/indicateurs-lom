<!--
SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: etalab-2.0
-->

# Avertissements sur les données
Nous indiquons ci-dessous divers avertissements généraux, relatifs à la qualité des données et à leur interprétation.
De manière générale, nous avons utilisé les indicateurs tels qu'ils sont fournis par l'ARPE, en prenant pour hypothèses simplificatrices qu'ils étaient à la fois exacts et comparables entre eux.
Ces hypothèses peuvent naturellement être contestées, et les avertissements ci-dessous ont également vocation à les nuancer.

## Données lacunaires
Certains indicateurs demandés par décret sont absents des sites des plateformes et des données fournies par l'ARPE. Nous ne sommes pas responsables de ces absences et utilisons les données telles que communiquées publiquement.

## Erreurs dans les données
Nous utilisons les données telles que communiquées publiquement, sans préjuger de leur exactitude.

## Temps d'attente
Le temps d'attente est définie par décret comme l'attente entre deux propositions de prestations (et non entre deux prestations acceptées). Nous considérons donc que l'indicateur d'attente publié par les plateformes est celui-là, et ne sommes pas responsables de l'interprétation différente ayant pu être faite par certaines plateformes.

## Indicateurs en journée, nuit, semaine, week-end
Le [décret d'application n° 2021-501 du 22 avril 2021](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043416674) distingue différents indicateurs en fonction des différentes plages horaires (jour, nuit) ou jours (semaine, week-end).
Cette distinction n'étant pas définie précisément, plusieurs interprétations sont possibles. Par exemple pour un travailleur effectuant entre 1 et 10 prestations par semaines, il est possible de comprendre que :
- l'indicateur de nuit doit traiter des travailleurs effectuant entre 1 et 10 prestations de nuit par semaine
- l'indicateur de nuit doit traiter des tâches effectuées la nuit, pour les travailleurs effectuant entre 1 et 10 tâches par semaine
- l'indicateur de nuit doit traiter des travailleurs effectuant entre 1 et 10 prestations par semaine, dont la majorité a lieu de nuit
- etc.

Les plateformes sont susceptibles d'avoir privilégié l'une ou l'autre de ces interprétations, ou plusieurs en fonction des années.

## Inclusion des bonus
Le décret impose d'inclure les bonus dans les rémunérations. Néanmoins, certaines plateformes indiquent sur leur site ne pas les avoir pris en compte pour certains indicateurs. Nous reportons les valeurs telles qu'indiquées, sans tenir compte de ces différences et en supposant que les indicateurs sont conformes au décret.
