<!--
SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: etalab-2.0
-->

# Analyse des indicateurs sur les plateformes de VTC et de livraison

## Contexte général
Selon la loi d'orientation des mobilités du 26 décembre 2019 créant [l’article L. 1326-3 du code des transports](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000039677959/), les plateformes de mobilité (VTC et livraisons de marchandises) sont soumises à l'obligation de publier sur leur site internet le 1er mars de chaque année une liste d’indicateurs relatifs à la durée d’activité et au revenu d’activité de ses travailleurs.

Les modalités de publication des indicateurs par les plateformes sont décrites dans [le décret d'application n° 2021-501 du 22 avril 2021](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043416674), [l'arrêté du 12 janvier 2022 relatif aux modalités de présentation des indicateurs](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045119086) et [l'arrêté du 12 janvier 2022 relatif à la méthode de détermination des valeurs non représentatives dans le calcul de l'indicateur relatif au temps d'attente des travailleurs](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045119079).

Ce dépôt propose, via des notebooks, différentes analyses et visualisations des indicateurs fournis dans ce contexte, collectés par l'ARPE et publiés [ici dans une première version](https://www.arpe.gouv.fr/actualites/indicateurs-sur-lactivite-des-travailleurs-des-plateformes-de-mobilite/) puis [ici dans une seconde version](https://www.arpe.gouv.fr/actualites/indicateurs-sur-lactivite-des-travailleurs-des-plateformes-de-mobilite-livraison/), repris tels que proposés dans la seconde version. Les indicateurs de 2023, publiés en 2024, sont analyés par l'ARPE [ici](https://www.arpe.gouv.fr/actualites/revenus-et-temps-de-travail-bilan-2023-sur-lactivite-des-travailleurs-chauffeurs-de-vtc-et-livreurs-des-plateformes-de-mobilite/).

À ce titre, n'hésitez pas à signaler toute incohérence éventuelle entre les données utilisées et celles fournies par l'ARPE.

## Structure du dépôt
Notablement, vous trouverez dans ce dépôt :
- Un fichier `avertissement.md` d'information générale sur la nature des données, la nature des indicateurs, et les incertitudes qu'ils peuvent porter
- Un dossier `notebooks/`, contenant quatre notebooks :
	- `traitement_donnees.ipynb` est dédié au traitement des données collectées, pour les transformer dans un format plus facilement exploitable pour la conception de graphes
	- `graphes_ARPE.ipynb` fournit le code ayant permis de concevoir les graphiques accompagnant la seconde version des données publiée par l'ARPE
	- `exploration_donnees.ipynb` permet une première exploration des données présentes et lacunaires
	- `analyse_donnees.ipynb` explore les données de manière à mettre en avant différents enjeux économiques
- Un dossier `data/`, contenant d'une part les données telles que fournies par l'ARPE, et d'autre part ces mêmes données retraitées par le notebook `traitement_donnees.ipynb`
- Un fichier de licence (le code fourni dans ce dépôt est sous licence MIT)
- Le dossier `graphes/`, vide, est destiné à accueillir les graphes que vous souhaitez sauvegarder lors de l'exécution des notebooks

## Exploration
Une exploration simple des notebooks est possible par l'interface graphique de ce site, en allant dans les fichiers textes (se terminant par `.txt` ou `.md` notamment) ou dans le dossier `notebooks/` pour les graphes et le code.

## Utilisation
Pour une utilisation active du code ou pour le modifier, commencez par installer toutes les dépendances :
```
python -m venv .venv
poetry install
source .venv/bin/activate
```

## Contribution
Pour contribuer au dépôt :
```
python -m venv .venv
poetry install
source .venv/bin/activate
pre-commit install
```
